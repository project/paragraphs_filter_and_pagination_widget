(function ($) {
  Drupal.behaviors.paragraphsFilterAndPaginationWidget = {
    attach: function (context, settings) {
      var $alphabeticFilterEl = $('#paragraph-filter-pagination-wrapper', context);
      var $table = $alphabeticFilterEl.find('table ', context);

      $alphabeticFilterEl.find('.tabledrag-toggle-weight-wrapper').hide();
      $table.find('td.field-multiple-drag a').hide();
      $table.find('tbody tr').each(function () {
        if ($(this).find('td:nth-child(2)').html().length === 0) {
          $(this).hide();
        }
      });
    },
  }
}(jQuery))
