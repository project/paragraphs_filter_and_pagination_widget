<?php

namespace Drupal\paragraphs_filter_and_pagination_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Plugin\Field\FieldWidget\InlineParagraphsWidget;


/**
 * A paragraphs filter and pagination widget.
 *
 * @FieldWidget(
 *   id = "paragraphs_filter_pagination",
 *   label = @Translation("Paragraphs filter & pagination"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class ParagraphsFilterPaginationWidget extends InlineParagraphsWidget {

  /**
   * Alphabetic filter ALL button value.
   */
  const ALPHABETIC_FILTER_ALL = 'ALL';

  /**
   * ID of the element that ajax will be taken place.
   *
   * @var string
   */
  private $_divWrapperId = 'paragraph-filter-pagination-wrapper';

  /**
   * List of supported field types.
   *
   * @var array|string[]
   */
  private $_supportedFieldTypes = [
    'string',
    'entity_reference',
  ];

  /**
   * Remove field from widget settings.
   *
   * @var array|string[]
   */
  private $_unsupportedWidgetSettings = [
    'edit_mode',
    'add_mode',
  ];

  /**
   * {@inheritDoc}
   */
  public static function defaultSettings(): array {
    $setting = parent::defaultSettings();

    // Limit the scope of the widget.
    $setting['edit_mode'] = 'open';
    $setting['add_mode'] = 'dropdown';
    $setting['filter_by'] = '';

    return $setting;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = parent::settingsForm($form, $form_state);

    // Only one paragraph supported.
    if (count($elements['default_paragraph_type']['#options']) > 1) {
      return [
        'not_supported' => [
          '#type' => 'markup',
          '#markup' => '<div><strong>' . $this->t('Multiple paragraph is not supported yet. Please select other widget for this field.') . '</strong></div>',
        ],
      ];
    }

    // Pull the only paragraph.
    $paragraph_name = $elements['default_paragraph_type']['#default_value'];

    // Generate list of fields that can be filtered.
    $supported_fields = [];
    $paragraph_fields = $this->_getParagraphFields($paragraph_name);
    foreach ($paragraph_fields as $field_name => $field_definition) {
      if (in_array($field_definition->getType(), $this->_supportedFieldTypes) && $field_definition->getTargetBundle() == $paragraph_name) {
        $supported_fields[$field_name] = $field_definition->get('label');
      }
    }

    $elements['filter_by'] = [
      '#type' => 'select',
      '#title' => $this->t('Filter by'),
      '#default_value' => $this->getSetting('filter_by'),
      '#options' => $supported_fields,
      '#empty_value' => '_none',
    ];

    // Limit the scope of the widget.
    unset($elements['default_paragraph_type']['#empty_value']);
    foreach ($this->_unsupportedWidgetSettings as $field_setting) {
      $elements[$field_setting]['#access'] = FALSE;
    }

    return $elements;
  }


  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    return parent::formElement($items, $delta, $element, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state): array {
    return parent::formMultipleElements($items, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL): array {
    $elements = parent::form($items, $form, $form_state, $get_delta);
    $elements['#prefix'] = '<div id="' . $this->_divWrapperId . '">';
    $elements['#suffix'] = '</div>';

    // Pull widget settings.
    $filter_field = $this->getSetting('filter_by');

    // If field to filter empty, let's return the default form.
    if (empty($filter_field)) {
      return $elements;

    }

    // Build alphabetic buttons.
    $buttons = [];
    if (!empty($elements['widget'])) {
      $buttons = $this->_buildAlphabeticFilterButton($elements['widget']);
    }
    $elements['widget']['alphabetic_buttons'] = [
      '#type' => 'container',
      'buttons' => $buttons,
    ];

    // Return default step if no step defined.
    if (empty($form_state->get('step')) && !empty($buttons)) {
      $form_state->set('step', $this->_getFirstAvailableButton($buttons));

      // Show default items.
      $elements['widget'] = $this->_alphabeticFilterShowItemByChar($elements['widget'], $form_state->get('step'));
    }

    // Alter remove js callback.
    for ($i = 0; $i <= $elements['widget']['#max_delta']; $i++) {
      if (!empty($elements['widget'][$i])) {
        $elements['widget'][$i]['top']['links']['remove_button']['#ajax']['callback'] = [
          $this,
          'removeJsCallback',
        ];
      }
    }

    //  Alter add more button callback.
    $elements['widget']['add_more']['add_more_button_student']['#ajax']['callback'] = [
      $this,
      'addMoreAjaxCallback',
    ];

    // Set template.
    $elements['widget']['#theme'] = 'widget_field_multiple_value_form';

    // Attach library.
    $elements['widget']['#attached']['library'][] = 'paragraphs_filter_and_pagination_widget/alphabetic-filter';

    return $elements;
  }

  /**
   * Ajax handler for filter button.
   *
   * When user click the button, other items that has no matching
   * first character will be hidden.
   *
   * @param array $elements
   * @param FormState $form_state
   *
   * @return array
   */
  public function filterAjaxCallback(array $elements, FormState $form_state): array {
    // If there is an error, stays at current step and show the error.
    if (empty($form_state->getErrors())) {
      $clicked_button = $form_state->getTriggeringElement();
      $step = $clicked_button['#value'];
      $elements[$this->fieldIdPrefix]['widget'] = $this->_alphabeticFilterShowItemByChar($elements[$this->fieldIdPrefix]['widget'], $step);
    }
    else {
      // Stay at current step.
      $step = $form_state->get('step');
      $elements[$this->fieldIdPrefix]['widget'] = $this->_alphabeticFilterShowItemByChar($elements[$this->fieldIdPrefix]['widget'], $step);

      // Show the error element in current step.
      for ($i = 0; $i <= $elements[$this->fieldIdPrefix]['widget']['#max_delta']; $i++) {
        if (!empty($elements[$this->fieldIdPrefix]['widget'][$i]['#children_errors'])) {
          $elements[$this->fieldIdPrefix]['widget'][$i]['#access'] = TRUE;
        }
      }
    }

    // Update the buttons.
    $elements[$this->fieldIdPrefix]['widget']['alphabetic_buttons']['buttons'] = $this->_buildAlphabeticFilterButton($elements[$this->fieldIdPrefix]['widget']);

    return $elements[$this->fieldIdPrefix];
  }

  /**
   * Submit handler for the alphabetic filter submit button.
   *
   * The handler will add the filter button value to a form_state.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function alphabeticFilterSubmit(array &$form, FormStateInterface $form_state) {
    $clicked_button = $form_state->getTriggeringElement();
    if (!empty($clicked_button)) {
      $chars = range('A', 'Z');
      foreach ($chars as $char) {
        if ($clicked_button['#value'] == $char) {
          $form_state->set('step', $char);
        }
      }

      if ($clicked_button['#value'] == static::ALPHABETIC_FILTER_ALL) {
        $form_state->set('step', static::ALPHABETIC_FILTER_ALL);
      }
    }

    // Rebuild the form.
    $form_state->setRebuild();
  }

  /**
   * Callback remove button
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function removeJsCallback(array $form, FormStateInterface $form_state): array {
    $element = parent::itemAjax($form, $form_state);
    $buttons = $this->_buildAlphabeticFilterButton($element);

    $processed_el = FALSE;
    if (!empty($form_state->get('step'))) {
      $processed_el = $this->_alphabeticFilterShowItemByChar($element, $form_state->get('step'));
      if (empty($processed_el)) {
        $processed_el = $this->_alphabeticFilterShowItemByChar($element, $this->_getFirstAvailableButton($buttons));
      }
    }

    $element = !empty($processed_el) ? $processed_el : $element;
    $element['alphabetic_buttons']['buttons'] = $buttons;

    return $element;
  }

  /**
   * Callback add more function.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return string[]
   */
  public function addMoreAjaxCallback(array $form, FormStateInterface $form_state): array {
    $elements = parent::itemAjax($form, $form_state);

    $field_name = $this->fieldIdPrefix;
    $filter_by = $this->getSetting('filter_by');
    $processed_el = $this->_alphabeticFilterShowItemByChar($elements[$field_name]['widget'], $form_state->get('step'));
    if (empty($processed_el)) {
      $buttons = $this->_buildAlphabeticFilterButton($elements[$field_name]['widget']);
      $processed_el = $this->_alphabeticFilterShowItemByChar($elements[$field_name]['widget'], $this->_getFirstAvailableButton($buttons));
    }
    $elements[$field_name]['widget'] = $processed_el;

    // Show all item where $filter_by is empty.
    // This will make sure the add form appear in the current step.
    for ($i = 0; $i <= $elements[$field_name]['widget']['#max_delta']; $i++) {
      $string = $this->_getFieldValue($elements[$field_name]['widget'][$i]);

      if (empty($string)) {
        $elements[$field_name]['widget'][$i]['#access'] = TRUE;
      }
    }

    $form_state->setRebuild();
    return $elements[$field_name];
  }

  /**
   * Helper to detect if the string given has matching first character.
   *
   * @param $string
   * @param $char
   *
   * @return bool
   */
  private function _firstCharacterMatch($string, $char): bool {
    return strtoupper(substr($string, 0, 1)) == strtoupper($char);
  }

  /**
   * Hide items by given first char.
   *
   * @param array $element
   * @param string $char
   *
   * @return array|bool
   */
  private function _alphabeticFilterShowItemByChar(array $element, string $char = '') {
    $has_visible_item = FALSE;
    $chars = range('A', 'Z');
    $filter_by_field = $this->getSetting('filter_by');

    // Enable all items.
    if ($char == static::ALPHABETIC_FILTER_ALL) {
      for ($i = 0; $i <= $element['#max_delta']; $i++) {
        $has_visible_item = TRUE;
        $element[$i]['#access'] = TRUE;
      }
    }
    else {
      foreach ($chars as $c) {
        if ($c == $char) {
          for ($i = 0; $i <= $element['#max_delta']; $i++) {
            $string = $this->_getFieldValue($element[$i]);
            $element[$i]['#access'] = FALSE;
            if ($this->_firstCharacterMatch($string, $c)) {
              $has_visible_item = TRUE;
              $element[$i]['#access'] = TRUE;
            }
          }
        }
      }
    }

    return $has_visible_item ? $element : FALSE;
  }

  /**
   * Build alphabetic filter buttons from given element.
   *
   * @param array $element
   *
   * @return array
   */
  private function _buildAlphabeticFilterButton(array $element): array {
    $chars = range('A', 'Z');
    $buttons = [];
    $visible_items = [];

    // Collect visible items for this character.
    foreach ($chars as $char) {
      for ($i = 0; $i <= $element['#max_delta']; $i++) {
        $string = $this->_getFieldValue($element[$i]);
        if ($this->_firstCharacterMatch($string, $char)) {
          if (!in_array($char, $visible_items)) {
            $visible_items[] = $char;
          }
        }
      }
    }

    if (!empty($visible_items)) {
      $chars[] = static::ALPHABETIC_FILTER_ALL;
    }

    if (!empty($element['alphabetic_buttons']['buttons'])) {
      $buttons = $element['alphabetic_buttons']['buttons'];
      foreach ($buttons as $index => $button) {
        if ($button['#value'] === static::ALPHABETIC_FILTER_ALL) {
          $buttons[$index]['#access'] = TRUE;
        }
        else {
          $buttons[$index]['#access'] = in_array($button['#value'], $visible_items);
        }
      }
    }
    else {
      foreach ($chars as $label) {
        $buttons[] = [
          '#name' => 'filter_' . strtolower($label),
          '#type' => 'submit',
          '#value' => $label,
          '#ajax' => [
            'callback' => [
              $this,
              'filterAjaxCallback',
            ],
            'wrapper' => $this->_divWrapperId,
            'method' => 'replace',
            'effect' => 'fade',
          ],
          '#submit' => [
            [
              $this,
              'alphabeticFilterSubmit',
            ],
          ],
          '#access' => $label == static::ALPHABETIC_FILTER_ALL || in_array($label, $visible_items),
        ];
      }
    }

    return $buttons;
  }

  /**
   * Return a value of the first available button.
   *
   * @param array $buttons
   *
   * @return string
   */
  private function _getFirstAvailableButton(array $buttons): string {
    foreach ($buttons as $button) {
      if ($button['#access'] === TRUE) {
        return $button['#value'];
      }
    }

    return static::ALPHABETIC_FILTER_ALL;
  }

  /**
   * Get fields of given paragraph name.
   *
   * @param string $paragraph_name
   *
   * @return mixed
   */
  private function _getParagraphFields(string $paragraph_name) {
    return \Drupal::service('entity_field.manager')
      ->getFieldDefinitions('paragraph', $paragraph_name);
  }

  /**
   * Get field value.
   *
   * @param array $element
   *
   * @return string
   */
  private function _getFieldValue(array $element): string {
    $filter_by_field = $this->getSetting('filter_by');
    $type = '';
    if (!empty($element['subform'][$filter_by_field]['widget']['#type'])) {
      // Select widget.
      $type = $element['subform'][$filter_by_field]['widget']['#type'];
    }
    elseif (!empty($element['subform'][$filter_by_field]['widget'][0]['target_id']['#type'])) {
      // Autocomplete.
      $type = $element['subform'][$filter_by_field]['widget'][0]['target_id']['#type'];
    }
    elseif (!empty($element['subform'][$filter_by_field]['widget']['target_id']['#type'])) {
      // Autocomplete with tag.
      $type = $element['subform'][$filter_by_field]['widget']['target_id']['#type'];
    }

    switch ($type) {
      case 'select':
        return $this->_getSelectWidgetDefaultValue($element['subform'][$filter_by_field]['widget']);

      case 'radios':
        return $this->_getRadiosWidgetDefaultValue($element['subform'][$filter_by_field]['widget']);

      case 'entity_autocomplete':
        return $this->_getAutoCompleteWidgetDefaultValue($element['subform'][$filter_by_field]['widget']);

      default:
        if (empty($element['subform'][$filter_by_field]['widget'][0]['value']['#default_value'])) {
          return '';
        }
        else {
          return $element['subform'][$filter_by_field]['widget'][0]['value']['#default_value'];
        }
    }
  }

  /**
   * Get select widget default value.
   *
   * @param array $widget
   *
   * @return string
   */
  private function _getSelectWidgetDefaultValue(array $widget): string {
    if ($widget['#type'] !== 'select' || (empty($widget['#default_value'][0]) && $widget['#default_value'][0] != 0)) {
      return '';
    }

    $valueID = $widget['#default_value'][0];
    if ($valueID !== NULL) {
      return $widget['#options'][$valueID];
    }

    return '';
  }

  /**
   * Get radios widget default value.
   *
   * @param array $widget
   *
   * @return string
   */
  private function _getRadiosWidgetDefaultValue(array $widget): string {
    if ($widget['#type'] !== 'radios' || (empty($widget['#default_value']) && $widget['#default_value'] != 0)) {
      return '';
    }

    $valueID = $widget['#default_value'];
    if ($valueID !== NULL) {
      return $widget['#options'][$valueID];
    }

    return '';
  }

  /**
   * Get autocomplete widget default value.
   *
   * @param array $widget
   *
   * @return string
   */
  private function _getAutoCompleteWidgetDefaultValue(array $widget): string {
    $default_value = NULL;
    if (!empty($widget[0]['target_id']['#default_value'])) {
      $default_value = $widget[0]['target_id']['#default_value'];
    }
    elseif (!empty($widget['target_id']['#default_value'])) {
      // Autocomplete with tag.
      $default_value = $widget['target_id']['#default_value'];
    }

    // For some reason, the #default_value on each ajax request is nested.
    $entity = get_class($default_value) ? $default_value : $default_value[0];
    if (get_class($entity) && method_exists($entity, 'label')) {
      return $entity->label();
    }

    return '';
  }

}
